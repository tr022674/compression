import os
import time

import numpy as np
from PIL import Image

from algorithoms import ppm

def encode(fn, rgb, sample_size):
    '''
    This function calls the relevent functions to compress the images 
    I use the PIL library to compress using BMP and PNG compressions
    I have implemented PPM and RLE from scratch
    '''
    rgb = np.array(rgb)[:sample_size, :sample_size, :]

    #time png compression
    t0 = time.perf_counter()
    Image.fromarray(rgb).save("encode.png")
    t1 = time.perf_counter()
    png_encode_time = t0 - t1

    #time bmp compression
    Image.fromarray(rgb).save("encode.bmp")
    t2 = time.perf_counter()
    bmp_encode_time = t2 - t1

    #create RGB channels
    r = rgb[:, :, 0].flatten()
    g = rgb[:, :, 1].flatten()
    b = rgb[:, :, 2].flatten()
    rgb = [r, g, b]

    #time RLE + PPM compression
    ppm_encode_time, ppm_decode_time,init_msg,target = ppm.wrap_encode(rgb, sample_size * 2, fn)

    # add all 3 r g b files for the total size
    ppm_size = sum([os.path.getsize(f"{fn}_{channel}.pickle") for channel in ['r','g','b']])
    png_size = os.path.getsize("encode.png")
    bmp_size = os.path.getsize("encode.bmp")

    #return useful statistics
    #compression_ratio, encode time, sample size, algorithm index
    return [(
        (bmp_size / bmp_size, bmp_encode_time, sample_size, 0),
        (bmp_size / ppm_size, ppm_encode_time, sample_size, 1),
        (bmp_size / png_size, png_encode_time, sample_size, 2),
    ),(init_msg,target,sample_size*2)]



def decode(name,priori):
    '''
    decode the image from a chanel file
    '''
    init_msg,target,k = priori
    table = ppm.load(name)
    decoded_msg, min_table = ppm.decode(table, init_msg, k, target)
    decoded = []
    
    for i in range(0,len(decoded_msg.split(',')),2):
        v = decoded_msg.split(',')[i]
        f = decoded_msg.split(',')[i+1]
        decoded.extend([v for _ in range(int(f))])
    return np.array(decoded).astype(int)



