import numpy as np
from PIL import Image
import pickle
import time


def encode(msg: str, k: int) -> list:
    """
    encode the msg with a k order model
    """
    #initialise the table with order 0 mapping
    table = [dict() for i in range(k + 2)]
   
    table[0][""] = dict()
    for i in range(2 ** 8):
        table[0][""][chr(i)] = 1

    def update(char, substring):
        """
        update context - expected char pairings
        """
        #update all substring tables and add counts
        for i in range(0, len(substring) + 1):
            pre = substring[i:]
            s = len(pre)
            if pre not in table[s + 1]:
                table[s + 1][pre] = dict()
            if char not in table[s + 1][pre]:
                table[s + 1][pre][char] = 0

            # add a 1 to the relationship pre,char
            table[s + 1][pre][char] += 1 

    for i in range(len(msg)):
        char = msg[i]
        start = i - k if i > k else 0
        context = msg[start:i]
        update(char, context)
    return table


def decode(table, substring, k, target_length):
    '''
    decoding method
    uses the unique constraint that a n order markov model 
    will have a single pairing this will be the next character 
    in the decoded message
    '''
    min_table = [{} for i in range(k)]
    decode_idx = k
    #we need an initial string
    decoded_msg = substring
    while True:
        for i in range(k):
            substring_slice = substring[-i:]

            if substring_slice:
                #attempt to retrive the pairing at i+1th model
                options = table[i + 1].get(substring_slice, None)
                if options is not None:
                    #unique constraint
                    if len(options.keys()) == 1:
                        #create a new table of all the queries
                        min_table[i + 1].update({substring_slice: options})
                        next_char = list(options.keys())[0]
                        decoded_msg += str(next_char)
                        substring += str(next_char)
                        substring = substring[1:]

                        #remove unique pairing 
                        table[i + 1].pop(substring_slice)
                        break
        else:
            #warn there is no unique pairing at maximum order model
            print(f"warning failed unique states retry with larger k")
            break
        
        #when the decoded message is equal to the target length stop decode
        if len(decoded_msg) >= target_length: 
            break

        decode_idx += 1

    return decoded_msg, min_table


def save(table, fn):
    with open(fn, "wb") as f:
        pickle.dump(table, f)

def load(fn):
    with open(fn, "rb") as f:
        unpickler = pickle.Unpickler(f)
        return unpickler.load()

def wrap_encode(rgb, k, fn):
    '''
    encode and decode function that returns 
    some stats with the compression
    '''
    encode_time = 0
    decode_time = 0

    for data_channel, channel_name in zip(rgb, ["r", "g", "b"]):
        # run length coding
        s_idx = np.where(np.diff(data_channel))[0] + 1
        RLE_msg = ",".join([f"{x[0]},{x.size}" for x in np.split(data_channel, s_idx)])

        t0 = time.perf_counter()
        table = encode(RLE_msg, k)

        # first pass to get the min reduced table
        
        decoded_msg, min_table = decode(table, RLE_msg[:k], k, len(RLE_msg))
        t1 = time.perf_counter()
        encode_time += t1 - t0
        print(min_table)
        # decode with reduced table
        t0 = time.perf_counter()
        decoded_msg, min_table = decode(min_table, RLE_msg[:k], k, len(RLE_msg))
        t1 = time.perf_counter()
        decode_time += t1 - t0

        #test the decode is equal to the original message
        print(decoded_msg == RLE_msg)

        #save the reduced table with pickle
        save(min_table, f"{fn}_{channel_name}.pickle")
    return encode_time, decode_time,RLE_msg[:k],len(RLE_msg)


if __name__ == "__main__":
    pass
