import numpy as np
import pandas as pd
import plotly.express as px
import streamlit as st
from PIL import Image
from plotly import express as px

from algorithoms import encode

st.title("IA compression")
image_files = st.file_uploader(
    "File Choosers",
    type=None,
    accept_multiple_files=True,
    key=None,
    help=None,
    on_change=None,
    args=None,
    kwargs=None,
)
st.write("we will now use some compression algorithoms to create some bench-marks")
compression_available = ["bmp", "rle+ppm", "png"]

st.write(f"available algorithoms {compression_available}")

if image_files is not None:
    for file in image_files:
        #load each image from file picker
        picture = Image.open(file)
        rgb = np.array(picture)

        #display image
        st.image(picture, width=500)

        #compress button
        button_clicked = st.button(f"Compress {file.name}")

        #run encoding at different samples
        stats = []
        for sample in (10,20):
            data,priori = encode.encode(file.name.split(".")[0], rgb, sample)
            stats.extend(data)
        df = pd.DataFrame(stats)
        df.columns = ["CR", "time", "sample", "algo"]

        if button_clicked:
            #display statistics on a scatter plot
            fig = px.scatter(
                df,
                x="CR",
                y="time",
                color="algo",
                title="Compression Ratio vs Encode Time",
            )
            st.plotly_chart(fig)

        decompress_button_clicked = st.button(f"Decompress {file.name}")
        if decompress_button_clicked:
            r = encode.decode(f'{file.name.split(".")[0]}_r.pickle',priori)
            g = encode.decode(f'{file.name.split(".")[0]}_g.pickle',priori)
            b = encode.decode(f'{file.name.split(".")[0]}_b.pickle',priori)
            rgb = np.concatenate([r,g,b],axis=0)
            st.image(rgb)
            
